## Tutorial

From [codeanddeploy.com](https://codeanddeploy.com/blog/laravel/laravel-8-authentication-login-and-registration-with-username-or-email)

## Stack

Laravel Framework 10.x-dev

PHP 8.2.0

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
